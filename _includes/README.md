# Assets

The `_includes` directory is used with abundance to allow for module adapations. Please alter these assets as deemed necessary. However, I would highly encourage reading [MSU Web Standards](https://comms.msu.edu/resources/msu-web-standards) prior to altering these assets.

## Status
Dynamic assets were intentionally designed for future  usage; please embed these assets via `{% include [asset].html %}` in both Markdown and HTML content. Static assets were designed for structured locations are likely not adequate for embedding. Editable is a relative standard for the ease of editing these assets and the likelihood of wanting to do so.

| Asset             | Status  | Editable      |
|-------------------|---------|---------------|
| content-skip.html | Static  | Not Suggested |
| footer.html       | Static  | Not Suggested |
| search.html       | Static  | Not Suggested |
| masthead.html     | Static  | Not Suggested |
| head.html         | Static  | Mutable       |
| divider.html      | Static  | Mutable       |
| icon.html         | Static  | Mutable       |
| posts.html        | Dynamic | Mutable |
| unit-title.html   | Dynamic | Mutable       |
| title.html        | Dynamic | Mutable       |
| jumbotron.html    | Dynamic | Mutable       |

<hr>

## content-skip.html

This is an asset used for improving the accessibility of keyboard users, including those with screenreaders. This enables the option to skip to the `id="content"` via tab quickly. There is little to alter, except changing the beginning `href` location.

## footer.html

This is an asset used for embedding MSU's standardized footer. To maintain MSU Web Standards compliance, this may need to be updated in the future. However, it is strongly discouraged to alter outside of [MSU's University Communications](https://comms.msu.edu/resources/msu-web-standards/footer)'s mandates as it may violate university directives.

## head.html

This is an asset used to embed `<head>` into each page. This will likely need to be altered to add additional SCSS/CSS files in the future.

## masthead.html

This is an asset used for embedding MSU's standardized masthead. To maintain MSU Web Standards compliance, this may need to be updated in the future. However, it is strongly discouraged to alter outside of [MSU's University Communications](https://comms.msu.edu/resources/msu-web-standards/masthead)'s mandates as it may violate university directives.

## divider.html

This is an asset used for embedding a navigation divider on `_posts`. This page will likely need future alteration to implement additional pages, when website grows. Likewise, this divider may be altered to appear on all pages, including `index.html` by `{% if %}` statements.

## icon.html

This will provide reference and metadata to third parties for displaying icons correctly. If icons are altered, please create a new directory in `/assets/icons` and not do alter `/assets/brand`.

## posts.html

This will provide all `_posts` in a formatted `<section>` for easily displaying all posts.

## unit-title.html

This will be your unit's title in the header. For more complicated unit naming schemes, it may be necessary to alter the HTML directly to ensure proper embedding. Please ensure compliance with [MSU Brand Studio](https://brand.msu.edu/logos/identity/signatures).

## title.html

This will return the webpages's `<title>` as a string. Please encapsulated as appropriate.

## jumbotron.html

This is the jumbotron class display. Please view the jumbotron.html document for proper usage. There exists a limitation where only one jumbotron may be disabled per page; this can be worked-around by copying the contents of `jumbotron.html` into the document and manually completing the requested information.

## search.html

This is an asset used for embedding MSU's standardized search box. To maintain MSU Web Standards compliance, this may need to be updated in the future. However, it is strongly discouraged to alter outside of [MSU's University Communications](https://comms.msu.edu/resources/msu-web-standards/search-tool)'s mandates as it may violate university directives. You may request for an [exemption to the single search box limit](https://comms.msu.edu/contact). However, without an exemption you may not embed additional search boxes.