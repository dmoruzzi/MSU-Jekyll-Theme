---
---
# MSU-Jekyll-Theme

This theme was designed to aid in the rapid development and deployment of websites for academic faculty at Michigan State University. While originally designed for the College of Engineering’s UNIX webspace, Jekyll produces static HTML content that may be readily hosted anywhere. This means that you may easily deploy to many hosting options, such as MSU Cascade or MSU’s shared web server.

This theme was designed to be very modular. Please visit `_includes/README.md` for information regarding available suggested information for included assets.


## Usage

Please edit `_config.yml` with the relevant information. It is critical to update this information to be within [MSU Web Standards](https://comms.msu.edu/resources/msu-web-standards) compliance.

You may edit `index.md` to alter the initial landing page.

You may add, delete, and edit `_posts` to alter the available posts for this website.


To actively develop, please execute to watch real-time edits:

    $ bundle exec jekyll s

### Completion

When completed, please build your website with the following:

    $ bundle exec jekyll b

All results will be published to

    _sites/

This will append the appropriate site URL for asset management.