---
title: "MSU-Jekyll-Theme"
layout: default

jumbotron_enable: true
jumbotron_title: "MSU-Jekyll-Theme"
jumbotron_desc: "This would be an excellent location for describing your organizational unit's function."
---

{% include jumbotron.html %}

## Mission Statement 

This theme was designed to aid in the rapid development and deployment of websites for academic faculty at Michigan State University. While originally designed for the College of Engineering's [UNIX webspace](https://www.egr.msu.edu/decs/help-support/how-to/engineering_homepage_setup), Jekyll produces static HTML content that may be readily hosted anywhere. This means that you may easily deploy to many hosting options, such as [MSU Cascade](http://cascade.msu.edu) or [MSU's shared web server](https://tech.msu.edu/technology/website-services/web-hosting/). 

This project makes intensive use of [Michael Ezzo](https://www.linkedin.com/in/michael-ezzo-69a81396/)'s MSUBootstrap v0.1.1. template.

## Content
{% include posts.html %}